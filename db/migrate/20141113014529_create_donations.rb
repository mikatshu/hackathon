class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.integer :donor_id
      t.decimal :value
      t.date :date

      t.timestamps
    end
  end
end
