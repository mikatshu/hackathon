json.array!(@donations) do |donation|
  json.extract! donation, :id, :donor_id, :value, :date
  json.url donation_url(donation, format: :json)
end
